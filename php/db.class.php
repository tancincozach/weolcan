<?php
class DB
{
    public     		

      $db_config= array();


	function __construct($db_settings  = array())
	{
		try {

			if(empty($db_settings)) throw new Exception("Please provide database configurations settings. ");
			if(!isset($db_settings['user'])) throw new Exception("Please provide user. ");
			if(!isset($db_settings['pass'])) throw new Exception("Please provide password . ");
			if(!isset($db_settings['host'])) throw new Exception("Please provide hostname . ");
			if(!isset($db_settings['db'])) throw new Exception("Please provide database. ");
			
			$this->db_config = $db_settings;
			
		} catch (Exception $e) {
			
				echo $e->getMessage();
		}
	}

	function connect()
	{
		try {
			$conn = new PDO("mysql:host=".$this->db_config['host'].";dbname=".$this->db_config['db']., $this->db_config['user'],$this->db_config['pass']);
			// set the PDO error mode to exception
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			echo "Connected successfully"; 
		}
		catch(PDOException $e)
		{
		  echo "Connection failed: " . $e->getMessage();
		}
	}

	function close()
	{

	}

	function free_result()
	{

	}

}


?>