<?php
set_time_limit(0);
session_start();

/**
*
*Author: Tancinco Zach G.
*
*
*/

require_once('RestApi.class.php');


class Ajax extends httpWorker
{

 public 

   $result_array = array() ,
   $properties = array() ,
   $boolean_properties = array( 'led' ,'opto_1','opto_2','relay' ),
   $arrayPost   = array(),
   $additional_prop = array( 'gps');   



 public function __construct( $api_key  = '', $organization = '')
 {

    $this->_endPoint    = 'https://open-api.devicewise.com/api';

    
    /* 

    $this->userName     = 'weolcan@iotzone.co';
    $this->userPass     = 'S1MenormalX72.';


    */

    $this->userName     = isset($_SESSION['username']) ? $_SESSION['username'] :'weolcan@iotzone.co';
    $this->userPass     = isset($_SESSION['password']) ? $_SESSION['password']: 'S1MenormalX72.';

    $this->arrayPost['api_key'] = $api_key;
    $this->arrayPost['organization'] = $organization;



 }


 public function checkLoginCredentials()
 {

    $this->userName     = $_SESSION['username'];
    $this->userPass     = $_SESSION['password'];
   
    $result = $this->auth();

    

    if(isset($result['response']['errorMessages']))
    {

        return array('error'=>$result['response']['errorMessages'],'login'=>false);
    } 
    else
    {
      $_SESSION['organization'] = $result['response']['auth']['params']['orgKey'];
        return array('login'=>true);
    }    
 }


/*
  Segregation of properties 
*/


public function initGeofence()
{
   $coordinates = array($this->result_array['loc']['lat'],$this->result_array['loc']['lng']);


     $this->properties['geofence']  = $this->checkExistGeoference($this->result_array['key'],$coordinates);
}

/*
  a method that retrieves the coordinates for the geoference or for the google map api.
*/
public function getDeviceCoordinates()
{
  if(isset($this->result_array['key']))
  {    
    $address = array();

  if(isset($this->result_array['loc']['addr']['streetNumber']) )
  {
    array_push($address,$this->result_array['loc']['addr']['streetNumber']);
  }

  if(isset($this->result_array['loc']['addr']['street']) )
  {
    array_push($address,$this->result_array['loc']['addr']['street']);
  }

  if(isset($this->result_array['loc']['addr']['city']) )
  {
    array_push($address,$this->result_array['loc']['addr']['city']);
  } 

  if(isset($this->result_array['loc']['addr']['state']) )
  {
    array_push($address,$this->result_array['loc']['addr']['state']);
  } 

    $this->properties['device_coordinates']['fulladdress'] = (count($address) > 0 ) ? implode(" ",$address) :"Geolocation is not available.";     
    $this->properties['device_coordinates']['lat'] = @$this->result_array['loc']['lat'];
    $this->properties['device_coordinates']['lng'] = @$this->result_array['loc']['lng'];
    

  }
}
/*
  A method that segrates properties  for the drill down graph .
*/
public function segregatePropertiesForDrillDownGraph()
{  
 if(isset($this->result_array['properties']))
  {

   $new_property     = array();   

    foreach($this->result_array['properties']  as $property_key=>$property_val)
    {
     if(!in_array( $property_key , $this->boolean_properties)  && !is_numeric($property_key) && !in_array( $property_key , $this->additional_prop))
      {
        if(strpos($property_key ,'_')!== false)
         {

           $ex_prop_Key = explode('_',$property_key);

           $property_name =  isset($ex_prop_Key[0]) ? $ex_prop_Key[0]: '' ; 

           if($property_name)
           {  
            if(array_key_exists( $property_name  , $new_property))
              {
               $new_property[$property_name][] = $property_val['value'];            
              }  
              else
              {
               $new_property[$property_name][0] = $property_val['value'];
              }        
           }
         }
         else
         {
           $new_property[$property_key][] = $property_val['value']; 
         }
       }
     }
    }
    
     $jsonArray = array();

      if(isset($new_property)  && count($new_property) > 0 )
      {
       foreach ($new_property  as $prop_key => $value)
       {

        $jsonArray['base'][]= array('name'=>$prop_key , 'y'=>array_sum( $new_property[$prop_key] ) ,'drilldown'=>$prop_key);
        
        if(isset($new_property[$prop_key]))
        {
          if(count($new_property[$prop_key] ) > 0)
          {
            $dummy_array  = array('name'=>$prop_key , 'id'=>$prop_key);
            $data_array   = array(); 
            $i=1;
           foreach ($new_property[$prop_key]   as $sub_prop_key => $sub_prop_val)
           {
               $data_array['data'][] = array($prop_key.'_'.$i ,$sub_prop_val );
               $i++;
           }
            $jsonArray['drill_down_data'][] =  array_merge( $dummy_array , $data_array);
         }
        }
       }                 
      } 
      $this->properties['drill_down_graph'] =$jsonArray;        

  }

/*
  a method that identifies if the returned property is a  device system properties or device input properties and properly segrated them to their respective table 
*/
public function segregateDeviceTableOutput()
{

 $thing_def_array  = $this->thing_def_find($this->result_array['defKey']);  
 
 $thing_def_properties = @$thing_def_array['properties'];

$this->getDeviceCoordinates();

 if(isset($this->result_array['properties']))
  {
    if(count($this->result_array['properties']) > 0 )
    {      
       foreach($this->result_array['properties'] as $propKey=>$property)
       {


         if(array_key_exists($propKey, $thing_def_properties))
         {          
            $prefix = isset($thing_def_properties[$propKey]['prefix']) ? $thing_def_properties[$propKey]['prefix']:$thing_def_properties[$propKey]['name'];

            if(is_numeric($propKey) || in_array( $propKey , $this->additional_prop))
                {

                   $this->properties['device_system_properties'][$propKey] = array('property_value'=>$property['value'],'prefix'=> $prefix ); 
                
                }
                else
                {
                 if(!in_array( $propKey , $this->boolean_properties) )
                   {
                     $this->properties['device_input_properties'][$propKey]  = array('property_value'=>$property['value'],'prefix'=> $prefix );
                   }
                   else
                   {
                     $this->properties['device_output_properties'][$propKey] = array('property_value'=>$property['value'],'prefix'=> $prefix );
                   }  
                }
         }          
       }
      
    }
  }
}

/*
  a method that segrates properties for Line Graph.
*/
public function segregateLineGraph()
{

 if(isset($this->result_array['properties']))
  {
    if(count($this->result_array['properties']) > 0 )
    {
    foreach($this->result_array['properties'] as $propKey=>$property)
    {
      if(!in_array( $propKey , $this->boolean_properties)  && !is_numeric($propKey)  && !in_array( $propKey , $this->additional_prop))
      {
        $this->properties['prop_view_list_line_graph'][]  = array('name'=>$propKey,'data'=>array($property['value']));
        $this->properties['property_line_graph'][]  = $propKey;      
      }
    }
  }
 } 
}
/*
  a method that segrates properties for Solid Graph.
*/

public function segregateSolidGraph()
{
   $dummy_array   = array();

 if(isset($this->result_array['properties']))
  {
    if(count($this->result_array['properties']) > 0 )
    {
    foreach($this->result_array['properties'] as $propKey=>$property)
    {
      if(!in_array( $propKey , $this->boolean_properties)  && !is_numeric($propKey)  && !in_array( $propKey , $this->additional_prop))
      {
        $dummy_array[$propKey]  = $property['value'];    
      }
    }

    $this->properties['property_solid_graph']  =  $dummy_array;

  }
 } 
}

/*
  Searches the serial number on the thing list
*/
public function SearchNonThing()
{
  $things_list_array = $this->thing_list();    


  $ctr = 0;
  $selected_array_id=0;

  foreach($things_list_array as $thing_sub_array)
  {
    if(isset($thing_sub_array['attrs']))
    {
      if(isset($thing_sub_array['attrs']['serialnumber']))
      {
        if(trim($thing_sub_array['attrs']['serialnumber']['value'])==trim($this->arrayPost['api_key']))
        {
         $selected_array_id=$ctr;
         $this->result_array = $things_list_array[$selected_array_id];    
         
        }   
      }      
    }
   $ctr++;
  }

  if(count($this->result_array) > 0 && isset($this->result_array['id']))
  {    
   return true;
  }
  else
  {
   return false;
  }
   
}

/*
  a method used as identifier for which method to be used on the tab
*/
public function segregatePropertiesByTab( $criteria )
{

  $this->identifyThingOrSerial(true);

  switch ($criteria){

     case 'line-graph':
          $this->segregateLineGraph();
            return  json_encode($this->properties);
      break;
      case 'gauge-graph':

          $this->segregateSolidGraph();
      
          return  json_encode($this->properties);
      break;
      case 'drill-down-graph':
           $this->segregatePropertiesForDrillDownGraph();  
             return  json_encode($this->properties);
      break;
      case 'google-map-api':
          $this->getDeviceCoordinates();
            return  json_encode($this->properties);
      break;

  }

}

public function check_and_set_pre_define_properties()
{
    
       $property_keys = isset($this->result_array['properties']) ? array_keys($this->result_array['properties']):array();

      if(!in_array('8001',$property_keys))
       {            
         $this->property_publish($this->result_array['key'], 8001,  10);
          $this->result_array = $this->thing_find($this->arrayPost['api_key']);  
       }

       if(!in_array('gps',$property_keys))
       {            
         $this->property_publish($this->result_array['key'], 'gps',  1);
          $this->result_array = $this->thing_find($this->arrayPost['api_key']);  
       } 
   
}

/* check if the posted value is a thing or a serial number */

 public function identifyThingOrSerial( $tab=false)
 {  
  if($this->arrayPost['api_key'])
   {      
     if($this->session_org_switch($this->arrayPost['organization']))
     {
         $this->result_array = $this->thing_find($this->arrayPost['api_key']); 


        if($tab==false) 
        {
          $this->check_and_set_pre_define_properties();
          $this->segregateDeviceTableOutput();          
          $this->initGeofence();
        }

        if(!isset($this->result_array['key']))
        {
         if($this->SearchNonThing()==false)
          { 
           return json_encode(array('error'=>'Thing not found!'));
          }
          else
          {
            if($tab==false) 
            {           
              $this->check_and_set_pre_define_properties();
              $this->segregateDeviceTableOutput();                
              $this->initGeofence();
            }
                      
            return  json_encode($this->properties);
          }
       }
       return  json_encode($this->properties);    
     }
     else
     {
      return json_encode(array('error'=>'Organization not found!'));
     }
   }    
  }
/*
  a method that pushes data / property value to the cloud.
*/
  public function publishRequest($api_key,$postedArray)
  { 
    if($this->property_publish($api_key, $postedArray['key'],  $postedArray['value'])){
        return json_encode(array('msg'=>'Data has successully submitted.  .'));
    }
    else
    {
       return json_encode(array('error'=>'Unable to sumbit data.'));
    }
  }


  public function checkExistGeoference($api_key)
  {
     
     $return  = array('exist'=>false);

     $geofence_name = array(strval($api_key.'-geofence'));

     $geofence_list  = $this->geofence_list();

      if(isset($geofence_list['result']) && count($geofence_list['result']) > 0)
      { 
         foreach($geofence_list['result'] as $row)
        {
           if(in_array($row['name'], $geofence_name))
           {                    

              $return  = array('exist'=>true);
           }                   
         }
       }
     
    
       return $return ;
  }
/*
  a method the creates trigger that's being used for the geofence and it's created on the cloud or devicewise
*/
  public function createTrigger($email,$geofence_key,$api_key)
  {
              $geofence_name = $api_key.'-geofence';

              $trigger_entry_config = array(

                  'key'=>$api_key,
                  'name'=>$api_key.'-entry',
                  'event_type'=>'geofence',
                  'event'=>array(
                                'geofenceEvent'=>'enter',
                                'geofenceKey'=>$geofence_key,
                                'thingKey'=>strval($api_key)
                              ),
                  'actions'=> array(
                                  'body'=>$api_key.'-entry'.$geofence_name,
                                  'subject'=>$api_key.'-entry',
                                  'to'=>$email,
                                  'type'=>'email.send'
                                )
                );

              $trigger_exit_config = array(
                  'key'=>$api_key,
                  'name'=>$api_key.'-exit',
                  'event_type'=>'geofence',
                  'event'=>array(
                                'geofenceEvent'=>'exit',
                                'geofenceKey'=>$geofence_key,
                                'thingKey'=>strval($api_key)
                              ),
                  'actions'=> array(
                                   'body'=>$api_key.'-exit'.$geofence_name,
                                  'subject'=>$api_key.'-exit',
                                  'to'=>$email,
                                  'type'=>'email.send'
                                )
                );



                  $result_trigger_enter = $this->trigger_create($trigger_entry_config);




                  $result_trigger_exit = $this->trigger_create($trigger_exit_config);


                 

                  if(isset($result_trigger_enter['id']) && $result_trigger_exit['id'])
                  {

                     $start_trigger_enter = $this->trigger_update(
                                                                  array(
                                                                      'id'=>$result_trigger_enter['id'],
                                                                      'start'=>true,
                                                                       'key'=>$api_key
                                                                  )
                                                                );


                     $start_trigger_exit  = $this->trigger_update(
                                                                  array(
                                                                      'id'=>$result_trigger_exit['id'],
                                                                      'start'=>true,
                                                                      'key'=>$api_key
                                                                  )
                                                                );
                      return true;
                  }
                  else
                  {
                      return false;
                  }


  }

/*
 a method that removes an existing trigger which is used for the geofence.
*/
  public function removeTrigger($api_key)
  {
    $trigger_list_array  = $this->trigger_list($api_key);

    $triggers_to_be_deleted = array( 
                                $api_key.'-entry',
                                $api_key.'-exit'
                              );




    $result  = array('deleted'=>false);

    if(isset($trigger_list_array['result']) && count($trigger_list_array['result']) > 0)
    {
       foreach($trigger_list_array['result'] as $row)
      {        

         if(in_array($row['name'], $triggers_to_be_deleted))
         {           
          $this->trigger_delete( array('key'=>$api_key,'id'=>$row['id']));
          $result  = array('deleted'=>true);
         }
       }
     }


    
     return $result;
  }

/*
  A method that removes existing geofence on devicewise .
*/
  public function removeExistingGeoference($api_key)
  {    
      $result_geofence = $this->delete_geofence( $api_key );  

       $trigger_result = $this->removeTrigger($api_key);  

       $this->property_publish($api_key, 'gps',  0);


      if($trigger_result['deleted']==true)
         {
          return json_encode(array('msg'=>'Geofence successully deleted  .'));
         }
         else{

           return json_encode(array('msg'=>'Geofence successully deleted but was not able to delete or update the Enter and Exit Trigger '));
         }    

  }
/*
  A method that creates geofence on devicewise .
*/
  public function createGeoference($api_key,$email,$coordinates)
  {
          $geofence= $api_key.'-geofence';


          $return_geofence = $this->create_radial_geoference($geofence,$api_key, $coordinates);
          
          if(isset($return_geofence['id']))
          {

             $trigger = $this->createTrigger($email, $return_geofence['id'],$api_key);

             
            $this->property_publish($api_key, 'gps',  1);


             if($trigger==true)
             {
              
              return json_encode(array('msg'=>'You have successfully set a geofence .'));
             }
             else{

               return json_encode(array('msg'=>'You have successfully set a geofence but was not able to create Enter and Exit Trigger '));
             }

           }
    }
}  


$ajax = new Ajax(@$_POST['thingkey'],strtoupper(@$_POST['organization']));


switch($_REQUEST['controller'])
{

 case "_publish_post":
  if (isset( $_SESSION['thing_key']))
   {   
    echo $ajax->publishRequest($_SESSION['thing_key'], array('key'=>@$_POST['key'],'value'=>@$_POST['value']));  
   }
 break;

 case "_create_geoference":

 if($_POST)
 {
    $posted_coordinates = @$_POST['coordinates'];
    $email = @$_POST['email'];

   if(strpos($posted_coordinates ,'|')!== false)
   {
      $coord = explode('|', $posted_coordinates);    
      echo $ajax->createGeoference($_SESSION['thing_key'],$email,$coord);  
   }
 }
 break;
 case "_remove_geoference":

    echo $ajax->removeExistingGeoference($_SESSION['thing_key']);           

 break;
 case "_publish":
    
   if (isset( $_SESSION['thing_key']))
   {   
     $value = ($_POST['value']=='true' ? 1:0);
     $ajax->publishRequest($_SESSION['thing_key'], array('key'=>@$_POST['key'],'value'=>$value));     
   }

 break;

 case "_request_tab_info":

          if($_REQUEST['tab'])
          {
             echo $ajax->segregatePropertiesByTab( str_replace('#','',$_REQUEST['tab'])); 
          }
 break;


 case 'checkLogin':

          try {
            
              if(!isset($_POST['uname']) && !isset($_POST['pass']))
              {
                throw new Exception("Please input Username/Password.");
              }
                
                


                $result = $ajax->checkLoginCredentials();

                if(isset($result['error']))
                {
                  throw new Exception("Invalid Username / Password");                  
                }

                $_SESSION['login'] = true;   

              echo json_encode(array('msg'=>'Login Successful.'));
                
          } catch (Exception $e) {

             echo  json_encode(array('error'=>$e->getMessage())) ;
                
          }

 break;

 default: 
        $json_output = $ajax->identifyThingOrSerial();         
        $_SESSION['thing_key'] = $ajax->result_array['key'];

        echo $json_output;
 break;


}
 
 
?>