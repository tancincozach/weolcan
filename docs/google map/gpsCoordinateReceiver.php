<?php
$newLat=$_POST['newLat'];
$newLng=$_POST['newLng'];
date_default_timezone_set('America/Chicago');
$dateTime=date('l jS \of F Y \a\t h:i:s A');

echo <<<EOT
<p>
<div class="alert alert-success fade in">
<button class="close" data-dismiss="alert">x</button>
New location: ($newLat,$newLng) sent on $dateTime
</div>
</p>
EOT;

file_put_contents('newLat.txt',$newLat);
file_put_contents('newLng.txt',$newLng);
?>