/*

Fraemwork references in code
http://www.highcharts.com/demo/column-negative
http://www.highcharts.com/demo/gauge-solid

*/
  
  
  var stop_request      = true , 
      chart_properties    =  []  ,
      color_properties    =  []   , 
      last_tab_active     = '#line-graph',
      graphs              = ['#line-graph','#drill-down-graph','#gauge-graph'],
      /*thingKey            = $('#input-thingkey').val(),        
      thingKey            = $('#organization').val(),        */
      posted_data         = {thingKey:$('#input-thingkey').val(),organization:$('#organization').val()}
      jsonMap             = {};
      intervalTime        =  10000;
      request_is_running  = false;

  var interval;
  var geocoder;

/******Configuration for property 3005*******/

  var gps_config  = {popover:'Geofence current position',title_display:'Geofence current position' }

/*************/
 $('#input-thingkey').val('');  

  
var google_map_api =
 {


       setMap:function (m) {
                  x = m.getZoom();
                  c = m.getCenter();
                  google.maps.event.trigger(m, 'resize');
                  m.setZoom(x);
                  m.setCenter(c);
              }
          ,
        getAddress: function (latLng) {
                    $('#geo_address').remove();
                    geocoder = new google.maps.Geocoder();  
                    geocoder.geocode( {'latLng': latLng},
                      function(results, status) {
                 
                        if(status == google.maps.GeocoderStatus.OK) {
                          if(results[0]) {                          
                                                        
                             $("<input type=\"hidden\"  id=\"geo_address\" value=\""+results[0].formatted_address +"\" >").appendTo('body');
                          }
                          else {                           
                              $("<input type=\"hidden\"  id=\"geo_address\" value=\"No Result\" >").appendTo('body');
                          }
                        }
                        else {
                          $("<input type=\"hidden\"  id=\"geo_address\" value=\""+status+"\" >").appendTo('body');
                        }

                      });
            
               }
            ,
            initGoogleMap:function( device_location){     
              
              $('#location').remove();

              try
              {
                if(device_location=='' || $.isEmptyObject(device_location)) throw "ERROR: Empty Coordinates for Google Map Api";


                console.log(device_location);


                var mapInitialPos = new google.maps.LatLng(device_location.lat, device_location.lng) ,
                         mapOptions = {
                                        center: mapInitialPos,
                                        zoom: 11,
                                        tilt:0,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                      },                                    
                               map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions),
                 newLocationMarker = new google.maps.Marker({
                                                        map: map,
                                                          position: mapInitialPos,
                                                          icon:new google.maps.MarkerImage("images/target-marker.png",
                                                          new google.maps.Size(32, 32),
                                                              new google.maps.Point(0,0),
                                                              new google.maps.Point(16, 16))
                                                          }),
                  newLocationInfoWindow=new google.maps.InfoWindow();



                  

                  newLocationInfoWindow.setContent(device_location.fulladdress);
                  newLocationInfoWindow.open(map,newLocationMarker);
                  google.maps.event.addListener(map,'click',function(e)
                  {
                   
                  newLocationInfoWindow.setContent("Loading coordinates.."); 
                   $('#location').remove();
                  $('<input type="hidden" id="location" value="'+device_location.lat+'|'+device_location.lng+'"/>').appendTo('body')
                  newLocationMarker.setPosition(e.latLng);  
                  newLocationInfoWindow.close();
                  
                  google_map_api.getAddress(e.latLng);

                   

                 setTimeout(function() {
                        newLocationInfoWindow.setContent("");                  
                        newLocationInfoWindow.setContent($('#geo_address').val());
                     },1000);
              

                  newLocationInfoWindow.open(map,newLocationMarker);
                  });   


                  this.setMap(map);                               
              }
              catch( err )
              {
                     alert(err.message||err);
              }
                 
 
            }            
}

    var rest = {


        Login:function( username,password)
        {
               try
              {
                if(username=='' ||  password=='') throw "ERROR: Please Input Username/Password";

                 $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'checkLogin',uname:username,pass:password},
                      dataType: "json",
                      beforeSend:function (JsonObj)
                      {
                        $('.msg').html("<img src='images/icons/loading.gif' width='20'>&nbsp; Please wait while the system is checking your credentials.");
                      }
                      ,
                      success:function( JsonObj ) {                         
                      

                        if(!$.isEmptyObject(JsonObj.error))
                        {                            
                            $('.msg').html('<span class="glyphicon glyphicon glyphicon-minus-sign " style="color:red"></span>&nbsp;<strong>ERROR :'+JsonObj.error+'</strong> ');    
                        }
                        else
                        {
                          $('.msg').html('Login Successful. Redirecting....');
                             setTimeout(function() {

                                    window.location='index.php';
                                                                   
                               }, 4000); 

                             setTimeout(function() {

                                      window.location='index.php';
                                                                   
                               }, 2000); 

                        }

                      },
                      error:function( JsonObj ){
                                  $('.msg').html('<span class="glyphicon glyphicon glyphicon-minus-sign " style="color:red"></span>&nbsp;<strong>ERROR : Unable to process login</strong> ');    
                      }
                    });
              }
               catch( err )
              {
                     alert(err.message||err);
              }
        },

        /* 
          A method that will return HTML STATIC HEADER 
       */
    
          HtmlHeaderTemplate:function(   category  )
          {
              switch( category ){

                    case 'table_output_1':

                            return  '<table class="table table-hover-color table-bordered publish_property">'
                                     +'<thead>'
                                    +'<tr>'
                                        +'<td style="width:90%">Device  Input Properties</td>'
                                        +'<td  style="width:10%" >Status</td>'
                                        +'</tr>'
                                    +'</thead>'
                                    +'<tbody>'
                                    +'</tbody>'
                                  +'</table>';
                    break;
                    case 'table_output_2':

                            return '<table class="table table-hover-color table-bordered publish_property">'
                                          +'<thead>'
                                          +'<tr>'
                                              +'<td  style="width:90%">device output properties</td>'
                                              +'<td  style="width:10%">Status</td>'
                                              +'</tr>'
                                          +'</thead>'
                                          +'<tbody>'
                                          +'</tbody>'
                                        +'</table>';

                    break;
                    case 'table_output_3':

                            return '<table class="table table-hover-color table-bordered publish_property system_properties">'
                                          +'<thead>'
                                          +'<tr>'
                                              +'<td style="width:90%">device system properties</td>'
                                              +'<td style="width:10%">Status</td>'
                                              +'</tr>'
                                          +'</thead>'
                                          +'<tbody>'
                                          +'</tbody>'
                                        +'</table>';

                    break;
                    case 'graph_output':

                             return    '<div class="col-md-12 chart-container">'
                                              +'<ul class="nav nav-tabs" id="graph-tab">'
                                                     +' <li><a href="#line-graph" data-toggle="tab">Graph 1</a></li>'
                                                      +'<li><a href="#gauge-graph" data-toggle="tab">Graph 2</a></li>'
                                                      +'<li><a href="#drill-down-graph" data-toggle="tab">Graph 3</a></li>'
                                                      +'<li><a href="#google-map-api" data-toggle="tab">Google Map </a></li>'
                                              +'</ul>'
                                              +'<div class="tab-content">'
                                                       +'<div id="line-graph" class="tab-pane fade in active">'
                                                           +'<div class="panel">'
                                                              +'<div class="panel-heading" style="text-transform:uppercase;color:white;background:#3A6F9F; font-size: 15px;font-weight: bold;">atx status input column graph</div>'
                                                              +'<div class="base-graph" style="width:100%;height: 400px; margin-bottom:10px"></div>'
                                                          +'</div>'
                                                      +'</div>'
                                                      +'<div id="gauge-graph" class="tab-pane fade in" >'
                                                            +'<div class="panel">'
                                                                +'<div class="panel-heading" style="text-transform:uppercase;color:white;background:#3A6F9F; font-size: 15px;font-weight: bold;">atx status input solid gauge graph</div>'
                                                                +'<div class="base-graph" style="min-height: 400px; "></div>'
                                                             +'</div>'
                                                      +'</div>'
                                                    +'<div id="drill-down-graph" class="tab-pane fade in" >'
                                                            +'<div class="panel">'
                                                                +'<div class="panel-heading" style="text-transform:uppercase;color:white;background:#3A6F9F; font-size: 15px;font-weight: bold;">atx status input drilldown graph</div>'
                                                                +'<div class="base-graph" style="width:100%;height: 400px; margin-bottom:10px"></div>'
                                                             +'</div>'
                                                    +'</div>'
                                                 +'<div id="google-map-api" class="tab-pane fade in" >'
                                                            +'<div class="panel">'
                                                                +'<div class="panel-heading" style="text-transform:uppercase;color:white;background:#3A6F9F; font-size: 15px;font-weight: bold;">Google Map</div>'
                                                                +'<div class="base-graph" id="map_canvas" style="width:100%;height: 400px; margin-bottom:10px">'

                                                                +'</div>'
                                                             +'</div>'
                                                    +'</div>'
                                      +'</div>';
                    break;
              }
          },

      /* 
          A method that will return static HTML element
       */
          HtmlStaticTemplate:function( element ){
              switch( element )
              {
                case 'data_refresh_switcher':


                  return  '<div class="row api-handler" style="margin-bottom:10px;">'
                        +'<div class="col-lg-12">'
                         +'<input class="api-switch"  checked data-toggle="toggle" data-off="Refresh off" data-on="Refresh on" data-onstyle="success" data-offstyle="danger" type="checkbox">'
                       +'</div>'
                    +'</div>';

                break;

                case 'property_not_Found':

                    return '<tr class="active">'
                                      +'<td colspan="2">No property found on this section.</td>'
                                    +'</tr>';
                break;

              }
          }
          ,


      /* 
          A method that will return system messages 
       */
          SystemHtmlMessage:function(  system_state , custom_message  )
          {

              switch( system_state )
              {
               case 'publish_success':

                      return    '<div class="alert alert-success" style="padding:7px">' 
                                  +'<span class="glyphicon glyphicon-ok"></span>'
                                  +'&nbsp;<strong>Loading :</strong> '+custom_message+'.....'
                             +'</div>';
              break;
              case 'success':

                      return    '<div class="alert alert-success">' 
                                  +'<span class="glyphicon glyphicon-ok"></span>'
                                  +'&nbsp;<strong>Loading :</strong> '+custom_message+'.....'
                             +'</div>';
                break;
                case 'processing':

                      return    '<div class="alert alert-info">' 
                                  +'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'
                                  +'&nbsp;<strong>Loading :</strong> Please wait a moment for the data is being fetched .....'
                             +'</div>';
                break;

              case 'sending_data':

                      return    '<div class="alert alert-info">' 
                                  +'<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>'
                                  +'&nbsp;<strong>Loading :</strong> Submitting data, Please wait a moment .....'
                             +'</div>';
                break;
                case 'error':
                        
                      return     '<div class="alert alert-danger waiting">'
                                     +'<span class="glyphicon glyphicon glyphicon-minus-sign "></span>&nbsp;<strong>ERROR :</strong> The system was unable to process your request'
                                  +'</div>';
                break;


                case 'custom_error':
                     return  '<div class="alert alert-danger waiting">'
                                     +'<span class="glyphicon glyphicon glyphicon-minus-sign "></span>&nbsp;<strong>ERROR :</strong>'+custom_message
                                  +'</div>';

                break;

                case 'custom_reminder':

                      return '<div class="alert alert-info"> <strong>Attention :</strong> '+custom_message+'</div>' ;   


                break;
              }

          }
          ,

      /* 
          A method that will render graph           
       */
          renderGraph:function( jsonObject ){


                  $('.chart-container').remove();           

                  $graphObject = this.HtmlHeaderTemplate('graph_output');$($graphObject).appendTo('.graph-container');
                   


                 $('<input type="hidden" id="location" value="'+jsonObject.device_coordinates.lat+'|'+jsonObject.device_coordinates.lng+'"/>').appendTo('body')
                 $('#graph-tab a[href="'+last_tab_active+'"]').tab('show');


                  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) 
                  {
               
                    $(window).trigger('resize');

                    last_tab_active = $(this).attr('href'); 

                    rest.executeSearchByTab(last_tab_active);

                  


                    e.preventDefault();   
                  });



                  $('a[data-toggle="tab"]').on('click',function(e)
                  {      
                          $(window).trigger('resize');
                          last_tab_active = $(this).attr('href'); 
                          e.preventDefault();   
                    });

       
        }
        ,
        /*
          a method that will create the Drill down Graph
        */
          renderDrillDownGraph :function( jsonObject )
          {             
               $('#drill-down-graph .base-graph ').highcharts({
                    chart: {
                        type: 'column',
                        backgroundColor: '#F5F5F5'
                              },
                              title: {
                                  text: ''
                              },
                              subtitle: {
                                  text: ''
                              },
                              xAxis: {
                                  type: 'category'
                              },
                              yAxis: {
                                  title: {
                                      text: ''
                                  }

                                  },
                                  legend: {
                                      enabled: false
                                  },
                                  plotOptions: {
                                      series: {
                                          borderWidth: 0,
                                          dataLabels: {
                                              enabled: true,
                                              format: '{point.y:.1f}'
                                          }
                                      }
                                  },

                              tooltip: {
                                  headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
                  },
                                      series: [{
                                          name: "ATX INPUT",
                                          colorByPoint: true,
                                          data: jsonObject.base
                                      }],
                              drilldown:{
                                series:jsonObject.drill_down_data   
                              }
                              
                    });
           }
        ,

        /* 
          A method that will create a Column Type Graph which is using the Hight Charts Plugin 
       */
        createColumnGraph:function( property_keys , property_values )
        {
                    
                $('#line-graph .base-graph').highcharts({
                chart: {
                        type: 'column',
                        backgroundColor: '#F5F5F5'
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: property_keys
                    },
                    credits: {
                        enabled: false
                    },
                    series:property_values
                }); 
             
        }
       /* 
          A method that will create a Solid Type Graph which is using the Hight Charts Plugin 
       */
        ,
        createSolidGraph:function( properties )
        {
              // color scheme for the solid gauge

                   var gaugeOptions = {

                                chart: {
                                    type: 'solidgauge',
                                    backgroundColor: '#F5F5F5'
                                },

                                title: null,


                               pane: {
                                    center: ['50%', '85%'],
                                    size: '60%',
                                    startAngle: -90,
                                    endAngle: 90,
                                    background: {
                                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                                        innerRadius: '60%',
                                        outerRadius: '100%',
                                        shape: 'arc'
                                    }
                                },

                                tooltip: {
                                    enabled: false
                                },

                                // the value axis
                                yAxis: {
                                    stops: [
                                        [0.1, '#55BF3B'], // green
                                        [0.5, '#DDDF0D'], // yellow
                                        [0.9, '#DF5353'] // red
                                    ],
                                    lineWidth: 0,
                                    minorTickInterval: null,
                                    tickPixelInterval: 200,
                                    tickWidth: 0,
                                    title: {
                                        y: -70
                                    },
                                    labels: {
                                        y: 16
                                    }
                                },

                                plotOptions: {
                                    solidgauge: {
                                        dataLabels: {
                                            y: 5,
                                            borderWidth: 0,
                                            useHTML: true
                                        }
                                    }
                                }
                            };

             $.each(properties,function(key,value)
              {
              $('<div class="row gauge-holder"></div>').appendTo('#gauge-graph .base-graph');
                 $element  = $('<div class="col-xs-12 col-md-4 "></div>').appendTo('.gauge-holder');
                 $element.highcharts(Highcharts.merge(gaugeOptions, {
                      yAxis: {
                          min:0,
                          max: 200,
                          title: {
                             text: key.toUpperCase()
                          }
                      },
                      credits: {
                          enabled: false
                      },
                          series: [{
                          name: key.toUpperCase(),
                          data: [value],
                          dataLabels: {
                              format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                  ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                                     '</div>'
                          },
                          tooltip: {
                              valueSuffix:key
                          }
                      }]

                  }));

              });
    
        }
        ,
        /*
         A method that will  gather or Retrieve  all values / properties with it's corresponding color that is generated by the Column TYpe Chart
        */
        AsignPropertiesAndColorsByChart:function()
        {
          $legends = $('.highcharts-legend-item');

                 $.each($legends,function(i,obj)
                {
                   var color = $(obj).find('rect').attr('fill');
                   var property = $(obj).find('text').text();
                   var prop_color  = $('#span'+property).css('background-color');

                  if(color.length  >  0 )
                  {
                 
                    if((prop_color == 'transparent') || (prop_color == 'rgba(0, 0, 0, 0)') ) {

                      $('#span'+property).css({backgroundColor:''+color,'font-weight':'bold' })   

                    }                     
                  
                  }            
                });

        }

        /*
          a method that will create a Table for the Property List
        */ ,
         appendTableProperties:function( jsonObject )
         {
          
       
          $('table.publish_property').remove();

            var $device_input_prop_table    =  $(rest.HtmlHeaderTemplate('table_output_1')).appendTo('.property_list');
            var $device_output_prop_table   =  $(rest.HtmlHeaderTemplate('table_output_2')).appendTo('.property_button');
            var $device_system_prop_table   =  $(rest.HtmlHeaderTemplate('table_output_3')).appendTo('.property_button');
            var counter= {'input':0,'output':0,'system':0,'email_input':0};    




                if(!$.isEmptyObject(jsonObject.device_input_properties))
              {

                 $.each(jsonObject.device_input_properties,function(key,value)
                  {               

                      var select_property_index = chart_properties.indexOf(key)  , colorBg =  color_properties[select_property_index];

                       if(value.property_value<=0)
                       {
                          tr_output='<tr   class="active " ><td ><label style="font-weight:bold"  data-toggle="popover" data-trigger="hover" data-content="'+value.prefix+'" >'+key+'</label></td><td class="text-center" ><span   alt="" class="label label-sm label-danger property-output" >'+value.property_value+'</span></td></tr>';
    
                       }
                       else 
                       {

                          tr_output='<tr  class="active "><td ><label style="font-weight:bold"  data-toggle="popover" data-trigger="hover" data-content="'+value.prefix+'" >'+key+'</label></td><td class="text-center" ><span   data-toggle="popover" data-trigger="hover"  lt="" id ="span'+key+'" class="label label-sm '+key.replace(/[0-9 | _ ]/g, '')+'">'+value.property_value+'</span></td></tr>';                                                                                           
                    
                       } 

                          
                     

                      if(tr_output)
                      {
                        $device_input_prop_table.children('tbody').append(tr_output);         

                      }

                     counter.input++;
     
                  });

            if(counter.input==0)
             {
                $device_input_prop_table.children('tbody').append(this.HtmlStaticTemplate('property_not_Found'));                                                                                                           
             }

            $('.property_list').show();                  
    
            
            }





          

  if(!$.isEmptyObject(jsonObject.device_output_properties))
  {

                  // Table that shows the properties which can push values to the api which has a boolean value      
     

     


      $.each(jsonObject.device_output_properties,function(key,value)
                  {
                     $device_output_prop_table.children('tbody')

        
                      .append(
                        '<tr class="active">'
                          +'<td><label style="font-weight:bold"  data-toggle="popover" data-trigger="hover" data-content="'+value.prefix+'" >'+key+'</label></td>'
                          +'<td class="text-center">'
                             +'<div class="checkbox checkbox-success">'
                               +'<input id="'+key+'"  type="checkbox"> <label for="checkbox1"> </label>'
                            +'</div>'
                          +'</td>'
                        +'</tr>'
                        );                                        

                       if(value.property_value ==0)
                        {
                         $('#'+key).removeAttr('checked');                      
                        }
                        else{
                         $('#'+key).attr('checked','checked');                      
                        }                    

                        $('#'+key).on('click',function(){                          
                          rest.executePubishrest($(this));
                        });
                        counter.output++;
                                                          
                });

              


  }



    if( counter.output==0)
                {
                    $device_output_prop_table.children('tbody')
        
                      .append(this.HtmlStaticTemplate('property_not_Found'));  

                }
            
             

          if(!$.isEmptyObject(jsonObject.device_system_properties))
          {
             $.each(jsonObject.device_system_properties,function(key,value)
                  {                     
                   
        

                    if(key=='gps')
                    {
                         $device_system_prop_table.children('tbody').append('<tr class="active">'
                                          +'<td><label style="font-weight:bold"  data-toggle="popover" data-trigger="hover"  data-content="'+gps_config.popover+'">'+gps_config.title_display+'</label></td>'
                                          +'<td class="text-center">'
                                              +'<div class="checkbox checkbox-success">'
                                                +'<input class="geofence" type="checkbox"> <label for="checkbox1" data-content="GPS ign on"> </label>'
                                              +'</div>'
                                          +'</td>'                                  
                                        +'</tr>');

                         if(!$.isEmptyObject(jsonObject.geofence))
                        {
                          if(jsonObject.geofence.exist==true)
                          {
                            //$('.geofence ').attr('checked','checked');
                            $('.geofence').prop('checked',true);
                           }
                           else
                           {
                            //$('.geofence ').removeAttr('checked');
                            $('.geofence').prop('checked',false);
                           }
                        } 
                    }
                    else
                    {
                          $device_system_prop_table.children('tbody').append(
                                '<tr id="system_output_'+key+'" class="active system_output_'+key+'">'
                                  +'<td><label style="font-weight:bold"  data-toggle="popover" data-trigger="hover" data-content="'+value.prefix+'" >'+key+'</label></td>'
                                  +'<td class="text-center">'
                                       +'<input id="'+key+'"  type="text"  style="width:40px" value="'+value.property_value+'">'
                                  +'</td>'
                                +'</tr>'
                              );    

                    }
                               
                        
                         current_interval_value = $('#8001').val();

                        $('#'+key) 
                            .on('keypress',function (e) {
                           //if the letter is not digit then display error and don't type anything
                             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {                              
                                     return false;
                             }

                         })
                        .on('change',function(){  
                                    
                             if($(this).attr('id')=='8001')
                             {
                              if($(this).val() < 5){
                                  alert('Minimum input is 5.');
                                  $(this).val(current_interval_value);
                                return false;
                                }  


                                                             

                               rest.switchRequest(false);
                              
                               $('.api-switch').bootstrapToggle('off'); 
                               
                               var seconds = parseInt($(this).val());
                               intervalTime = seconds * 1000;                          

                             }
                             else{
                               if($(this).val() > 10){
                                alert('Maximum input is 10.');
                                $(this).val(0);
                                return false;
                             }   
                            }

                           current_interval_value = $(this).val();

                            rest.executePubish( {'key':$(this).attr('id'),'value':$(this).val()} );

                        });  

                        counter.system++;                                                               
      
      
                });
            
          }

                if( counter.system==0)
                {
                    $device_system_prop_table.children('tbody')
        
                      .append(this.HtmlStaticTemplate('property_not_Found'));  

                }



          var validEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/



                     $('.geofence ').on('click',function(){


                          if($(this).is(':checked'))
                          {
                               var tpl = '<div class="modal fade">'
                                +'    <div class="modal-dialog modal-md">'
                                +'        <div class="modal-content">'
                                +'            <div class="modal-header">'
                                +'                <h4 class="modal-title">Geofence</h4>'
                                +'            </div>'
                                +' <p class="msg-geo"></p>'
                                +'            <div class="modal-body">'
                                +'                <p><label>Email </label><input type="email"  class="form-control"  value=""></p>'
                                +'            </div>'
                                +'            <div class="modal-footer">'
                                +'     <button type="button" class="btn btn-default close-send-geofence" data-dismiss="modal">Close</button>'
                                +'     <button type="button" class="btn btn-default send-geofence" >Send Geofence</button>'
                                +'            </div>'
                                +'        </div><!-- /.modal-content -->'
                                +'    </div><!-- /.modal-dialog -->'
                                +'</div><!-- /.modal -->';


                                $(tpl).modal({keyboard:false, backdrop:false})
                                .on('hidden.bs.modal', function (e) {
                                    $(this).remove();
                                     
                                }); 

                              $email =  $('[type="email"]');
                              $email.change(function () {

                                  var isValid =this.value.match(validEmail),
                                      isInternet = this.value.match(/@.*\./);


                                  if(!isValid)
                                  {                                                                                                    
                                    weolcan.common.alert({message:'Invalid Email Format'})                             

                                    $(this).val('').focus();
                                  }
                              });

                              $('.send-geofence').click(function(){
                                   if( $email.val()=='')
                                   {                                     
                                      weolcan.common.alert({message:'Please input email'})                             
                                      return false;
                                   }



                                    rest.executeCreateGeo({email:$email.val(),coordinates:$('#location').val()});


                              });

                              $('.close-send-geofence').on('click',function(){

                                  $('.geofence').prop('checked',false);

                              })

                          }
                          else{

                            var tpl = '<div class="modal fade">'
                                +'    <div class="modal-dialog modal-md">'
                                +'        <div class="modal-content">'
                                +'            <div class="modal-header">'
                                +'                <h4 class="modal-title">Geofence</h4>'
                                +'            </div>'
                                +' <p class="msg-geo"></p>'
                                +'            <div class="modal-body">'
                                +'                <p><label>Are you sure you want to remove geofence ? </label></p>'
                                +'            </div>'
                                +'            <div class="modal-footer">'
                                +'                <button type="button" class="btn btn-success removegeofence" data-dismiss="modal">Okay</button>'
                                +'                <button type="button" class="btn btn-danger cancel"   data-dismiss="modal" >Cancel</button>'
                                +'            </div>'
                                +'        </div><!-- /.modal-content -->'
                                +'    </div><!-- /.modal-dialog -->'
                                +'</div><!-- /.modal -->';


                                $(tpl).modal({keyboard:false, backdrop:false})
                                .on('hidden.bs.modal', function (e) {
                                    $(this).remove();
                                     
                                }); 

                                  $('.removegeofence').on('click',function(){

                                    rest.removeGeoference({coordinates:$('#location').val()});

                                  });


                                  $('.cancel').on('click',function(){
                                         $('.geofence').prop('checked',true);
                                  });

                                    
                          }
            });
   
                

             $('[data-toggle="popover"]').popover();   
         } 
         ,
          /* 
          A method that will create div elements on the top portion
           that represents properties that displays numeric values 
          */

          appendProperties :function(jsonObject)
          {

                 this.appendTableProperties(jsonObject);

                // Switch buttons for the Start / Stop Api 

                $('.api-handler').remove();
                
                $( this.HtmlStaticTemplate('data_refresh_switcher') ).appendTo('.api-controller');

                if(stop_request==true)
                {
                  $('.api-switch').bootstrapToggle('off');   
                }
                else
                {
                  $('.api-switch').bootstrapToggle('on');   
                }
                
              $('.api-switch').on('change',function()
               {
                  rest.switchRequest($(this).prop('checked'));
               });
 
          }
           ,   
           /* 
            a method that will do an ajax request and pushes data to the cloud 
           */
            executePubishrest:function( object )
            {
                var object = $(object);
                $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_publish',key:object.attr('id') ,value:object.is(':checked')},
                      dataType: "json",
                      success:function( JsonObj ) {                         
                     

                      },
                      error:function( JsonObj ){
                                     
                      }
                    });
            }
            ,   
           /* 
            a method that will do an ajax request and pushes data to the cloud 
           */
            executePubish:function( postedValue  )
            {
                if(!$.isEmptyObject(postedValue.key) &&  !$.isEmptyObject(postedValue.value))
                {
                   $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_publish_post',key:postedValue.key ,value: postedValue.value},
                      dataType: "json",                      
                      beforeSend:function()
                      {
                        $('.system_message').html( rest.SystemHtmlMessage('sending_data') );
                      },
                      success:function( JsonObj ) {                         
                     
                           
                          if(!$.isEmptyObject(JsonObj.msg))
                          {
                            $('.system_message').html( rest.SystemHtmlMessage('publish_success' ,JsonObj.msg ) );

                          }
                          else
                          {
                           $('.system_message').html( rest.SystemHtmlMessage('custom_error' ,JsonObj.error ) ); 
                          }

                               setTimeout(function() {

                                   $('.system_message').html('');
                                                                   
                               }, 2000); 
                      },
                      error:function( JsonObj ){
                                     
                      }
                    });

                }
       
            }
               ,   
           /* 
            a method that will do an ajax request and pushes data to the cloud 
           */
            executeCreateGeo:function( postedValue  )
            {
               if(!$.isEmptyObject(postedValue.email) &&  !$.isEmptyObject(postedValue.coordinates))
                {
                   $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_create_geoference',email:postedValue.email,coordinates:postedValue.coordinates},
                      dataType: "json",
                      beforeSend:function()
                      {
                          $('.msg-geo').html(rest.SystemHtmlMessage('sending_data') );
                          $('.close-send-geofence').attr('disabled','disabled');
                      },
                      success:function( JsonObj ) {                         
                        
                          $('.msg-geo').html('');
                          if(!$.isEmptyObject(JsonObj.msg))
                          {
                            $('.close-send-geofence').removeAttr('disabled');
                            $('.msg-geo').html( rest.SystemHtmlMessage('success' ,JsonObj.msg ) );

                          }
                          else
                          {
                           $('.msg-geo').html( rest.SystemHtmlMessage('custom_error' ,JsonObj.error ) ); 
                          }

                           setTimeout(function() {

                                   $('.msg-geo').html('');
                                                                   
                               }, 2000); 

                           setTimeout(function() { 

                               $('.modal').modal('hide');


                         }, 2000); 

                      },
                      error:function( JsonObj ){
                          alert('Cannot request');
                      }
                });
              }
            }
                ,   
           /* 
            a method that will the remove the geofence
           */
            removeGeoference:function( postedValue  )
            {
               if(!$.isEmptyObject(postedValue.coordinates))
                {
                   $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_remove_geoference',coordinates:postedValue.coordinates},
                      dataType: "json",
                      beforeSend:function()
                      {
                        weolcan.common.alert_on_process({title:'Please wait..',message:'Removing Geofence'}); 
                      },
                      success:function( JsonObj ) {                         
                        $('.modal').modal('hide');
                            weolcan.common.alert({message:JsonObj.msg}); 
                            /*var ajax_request_object = rest.ajaxRequestNonInterval();                      */


                      },
                      error:function( JsonObj ){
                          alert('Cannot request');
                      }
                });
              }
            }
            ,                   
            /* 
              a method that will do an ajax request ,
              gathers data from the cloud  and calls methods to render graph and 
              append properties to be shown on the page 
             */
            executeSearchrest : function( propType ){



                var AjaxObj = $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:propType},
                      dataType: "json",
                      beforeSend:function()
                      {

                        $('#api_result,.system_message').show(); $('table.publish_property ,div.property_list > table').remove(); $('.api-controller , #sum_box , .prop-buttons , .graph-container').html(''); 


                        $('.system_message').html(   rest.SystemHtmlMessage('processing') );

                      }
                      ,
                      success:function( JsonObj ) { 
                      $('.system_message').html('');
                          if(JsonObj.error)
                          {
                            $('.system_message').html( rest.SystemHtmlMessage('custom_error' ,JsonObj.error ) );
                          }
                          else{
                            rest.renderGraph(JsonObj); 
                          
                            rest.appendProperties(JsonObj);  

                            rest.AsignPropertiesAndColorsByChart();                          
                          }                
                      }
                    })
                    .done(function(){
                        $('.api-switch').bootstrapToggle('enable');
                    })
                    .fail(function(){
                        $('.system_message').html();
                    });
            },

            identifyGraphTobeRendered:function(jsonObject,selected_tab){
              
                  try{
                      
                      if(selected_tab=='') throw "ERROR: Please select a tab";

                       switch (selected_tab)
                        {

                           case '#line-graph':
                               if($.isEmptyObject(jsonObject.device_system_properties) && $.isEmptyObject(jsonObject.prop_view_list_line_graph))
                                throw "ERROR: Unable to load grid due to empty coordinates.";
                                rest.createColumnGraph(jsonObject.property_line_graph,jsonObject.prop_view_list_line_graph);                                                                    
                            break;
                            case '#gauge-graph':
                            if($.isEmptyObject(jsonObject.property_solid_graph))
                                throw "ERROR: Unable to load grid due to empty coordinates.";
                                rest.createSolidGraph(jsonObject.property_solid_graph);                                    
                            break;
                            case '#drill-down-graph':
                              if($.isEmptyObject(jsonObject.drill_down_graph))
                                throw "ERROR: Unable to load grid due to empty coordinates.";
                                rest.renderDrillDownGraph(jsonObject.drill_down_graph);  

                            break;
                            case '#google-map-api':
                               if($.isEmptyObject(jsonObject.device_coordinates))
                                throw "ERROR: Unable to load grid due to empty coordinates.";
                                  jsonMap = jsonObject.device_coordinates;
                                  google_map_api.initGoogleMap(jsonMap);                                                  
                            break;

                        }

                    }
                  catch( err )
                  {                         
                     weolcan.common.alert({message:err.message||err})     
                  }                  
          
            },
 /* 
              a method that will do an ajax request ,
              gathers data from the cloud  and calls methods to render graph and 
              append properties to be shown on the page 
             */
            executeSearchByTab : function( current_tab ){



                var AjaxObj = $.ajax({ 
                      url:'php/ajax.php',                      
                      type:'POST',
                      data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_request_tab_info',tab:current_tab},
                      dataType: "json",
                      beforeSend:function()
                      {                        
                                                 
                          if(!!~jQuery.inArray(current_tab, graphs))
                          {
                              if($(current_tab+' .base-graph').highcharts())
                              {
                                $(current_tab+' .base-graph').highcharts().destroy();                          
                              } 

                          } else {
                              $(current_tab+' .base-graph').empty();
                          }
                          
                          $(current_tab+' .base-graph').html(rest.SystemHtmlMessage('processing') );

                      }
                      ,
                      success:function( JsonObj ) { 
                        
                         
                            $(current_tab+' .base-graph').empty();

                          if(JsonObj.error)
                          {
                            $(current_tab+' .base-graph').html(rest.SystemHtmlMessage('custom_error' ,JsonObj.error ) );                            
                          }
                          else
                          {                            
                            $(current_tab+' .base-graph').html('');
                          }                
                      }
                    })   
                    .done(function( jsonObject ){

                        $('.api-switch').bootstrapToggle('enable');                         

                         rest.identifyGraphTobeRendered(jsonObject,current_tab);

                         rest.AsignPropertiesAndColorsByChart();
         
                  
                    })
                    .fail(function( )
                    {
                        $(current_tab+' .base-graph').html(rest.SystemHtmlMessage('custom_error' ,'Error : Unable to load data.') );
                    });
                    
            },
            ajaxRequest: function ()
            {
                              $('.system_message').html( rest.SystemHtmlMessage('custom_reminder' ,'Data is updated in 30 second intervals.....' ));

                              return  $.ajax({ 
                                          url:'php/ajax.php',                      
                                          type:'POST',
                                          data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_retrieve'},
                                          dataType: "json",
                                          beforeSend:function()
                                          {
                                            request_is_running = true;
                                            $('.api-switch').bootstrapToggle('disable');                        

                                            $('#api_result,.system_message').show(); 

                                            $('.system_message').html( rest.SystemHtmlMessage('processing') );
                                          }
                                          ,
                                          success:function( JsonObj )
                                          { 
                                             request_is_running=false;
                                              $('.system_message').html('');
                                              $('.api-switch').bootstrapToggle('enable');
                                              if(JsonObj.error)
                                              {
                                               $('.system_message').html(rest.SystemHtmlMessage('custom_error',JsonObj.error)) ;
                                              }
                                              else
                                              {                                                
                                                $('#sum_box').html("");    
                                                rest.appendProperties(JsonObj);  
                                                rest.AsignPropertiesAndColorsByChart();                          
                                               }                
                                              }
                                        })                    
                                        .fail(function(){
                                            $('.system_message').html(rest.SystemHtmlMessage('error'));
                                        });
                                   
                                   }
            ,
            ajaxRequestNonInterval: function ()
            {
                              $('.system_message').html( rest.SystemHtmlMessage('custom_reminder' ,'Updated data is loaded to the page.....' ));

                              return  $.ajax({ 
                                          url:'php/ajax.php',                      
                                          type:'POST',
                                          data:{thingkey:posted_data.thingKey,'organization':posted_data.organization,controller:'_retrieve'},
                                          dataType: "json",
                                          beforeSend:function()
                                          {
                                            request_is_running = true;
                                            $('.api-switch').bootstrapToggle('disable');                        

                                            $('#api_result,.system_message').show(); 

                                            $('.system_message').html( rest.SystemHtmlMessage('processing') );
                                          }
                                          ,
                                          success:function( JsonObj )
                                          { 
                                             request_is_running=false;
                                              $('.system_message').html('');
                                              $('.api-switch').bootstrapToggle('enable');
                                              if(JsonObj.error)
                                              {
                                               $('.system_message').html(rest.SystemHtmlMessage('custom_error',JsonObj.error)) ;
                                              }
                                              else
                                              {                                                
                                                $('#sum_box').html("");    
                                                rest.renderGraph(JsonObj); 
                                               
                                                rest.appendProperties(JsonObj);              

                                               }                
                                              }
                                        })                    
                                        .fail(function(){
                                            $('.system_message').html(rest.SystemHtmlMessage('error'));
                                        });
                                   
                                   }
            ,
              executerestInterval : function( propType ){


                  intervalTime = ($('#8001').length > 0 ? parseInt($('#8001').val()) * 1000:10000);
                  
                  var ajax_request_object = rest.ajaxRequest();

                

                      interval = setInterval(
                                  function()
                                   {
                                      ajax_request_object.done(function(){
                                        rest.ajaxRequest();                                     
                                     });                                      
                                    },
                        intervalTime);  

                  setTimeout(function() {

                          rest.switchRequest(false);
                     

                     }, 300000);        
            }  
            ,
            switchRequest:function( switcher)
            { 
                  if(switcher==true)
                  {
                      stop_request = false;   

                      rest.executerestInterval();      

                  }
                  else
                  {
                    stop_request=true;
                    clearInterval(interval);                           

                  }
            },
          
            /* Initialization of the rest Api */
            init:function()            
            {
               
                $('.fa-search').click(function(event)
                {

                  posted_data         = {thingKey:$('#input-thingkey').val(),organization:$('#organization').val()}

                  if(posted_data.thingKey!='')
                  {
                    rest.executeSearchrest('_retrieve');
                  }                  
                  event.preventDefault();
                });


                 $('#input-thingkey , #organization').keyup(function(event) {
                  
                    if (event.keyCode == 13)
                    {
                     $('.fa-search').trigger('click');
                    }
                });

            }
        }




 //Self-Executing Anonymous Function
 //Encapsulate by weolcan
 ;(function(weolcan, $, undefined) {
  
    weolcan.processing = false;

    weolcan.common = {

        confirm: function(msg){
            
            var str = msg || 'Confirm Submit';

            if( confirm(str) ){
                return true;
            }

            return false;
        },

        alert: function(opts){

            var defaults = $.extend({                
                title: 'Alert',
                message: "Alert"
            }, opts );

            var tpl = '<div class="modal fade">'
            +'    <div class="modal-dialog modal-md">'
            +'        <div class="modal-content">'
            +'            <div class="modal-header">'
            +'                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
            +'                <h4 class="modal-title">'+defaults.title+'</h4>'
            +'            </div>'
            +'            <div class="modal-body">'
            +'                <p>'+defaults.message+'</p>'
            +'            </div>'
            +'            <div class="modal-footer">'
            +'                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
            +'            </div>'
            +'        </div><!-- /.modal-content -->'
            +'    </div><!-- /.modal-dialog -->'
            +'</div><!-- /.modal -->';

            $(tpl).modal({keyboard:false, backdrop:false})
            .on('hidden.bs.modal', function (e) {
                $(this).remove();
            }); 

        }
        ,
        alert_on_process:function(opts){
            var defaults = $.extend({                
                title: 'Processing..',
                message: "Please wait, processing..."
            }, opts );

            var tpl = '<div class="modal fade">'
            +'    <div class="modal-dialog modal-md">'
            +'        <div class="modal-content">'
            +'            <div class="modal-header">'            
            +'                <h4 class="modal-title">'+defaults.title+'</h4>'
            +'            </div>'
            +'            <div class="modal-body">'
            +'                <p>'+defaults.message+'</p>'
            +'            </div>'
            +'        </div><!-- /.modal-content -->'
            +'    </div><!-- /.modal-dialog -->'
            +'</div><!-- /.modal -->';

            $(tpl).modal({keyboard:false, backdrop:false})
            .on('hidden.bs.modal', function (e) {
                $(this).remove();
            }); 
        }
    }


}(window.weolcan = window.weolcan || {}, jQuery));



$(function () {
        
    rest.init();



});

