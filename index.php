<?php
session_start();

/*if(!isset($_SESSION['login']))
 {
    header('Location: login.php');
 }
*/
if(isset($_REQUEST['logout']))
{
    session_destroy();
    header('Location: login.php');  
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>M2Mzone Weolcan</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/icons/favicon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/icons/favicon-114x114.png">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link type="text/css" rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" rel="stylesheet" href="styles/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="styles/bootstrap/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="styles/animate.css">
    <link type="text/css" rel="stylesheet" href="styles/main.css">
    <link type="text/css" rel="stylesheet" href="styles/style-responsive.css">
    <link type="text/css" rel="stylesheet" href="styles/bootstrap-toggle.css"> 
    <link type="text/css" rel="stylesheet" href="styles/awesome-bootstrap-checkbox.css"> 

 <style>
     .on{
        color:#4CAF50 !important;
        font-weight:bold;
     }
     .off{
        color:#D9534F !important;
        font-weight:bold;
     }
 </style>
</head>
<body>
    <div>
                
        <!--BEGIN BACK TO TOP-->
        <a id="totop" href="#"><i class="fa fa-angle-up"></i></a>
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
        <div id="header-topbar-option-demo" class="page-header-topbar">
            <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" class="navbar navbar-default navbar-static-top">
                    <div class="navbar-header">                
                        <a href="docs/help.txt" class="weolcan-help" target="_blank"/>
                            <span class="glyphicon glyphicon-question-sign" ></span>
                        </a>
                        <a id="logo" href="index.php" class="navbar-brand">
                                <span class="fa fa-rocket"></span>
                                <span class="logo-text">Weolcan</span>
                                <span style="display: none" class="logo-text-icon">µ</span>
                        </a>
                    </div>
            <div class="topbar-main">
            <ul class="nav navbar navbar-top-links navbar-right mbn">
                    <li class="dropdown topbar-user" style="color:white;font-weight:bold">&nbsp;<span class="hidden-xs"><?php echo $_SESSION['organization'];?></span>&nbsp;</a>
                      
                    </li>
                    <li class="dropdown"><a data-hover="dropdown" href="?logout=1" class="dropdown-toggle"  title="<?php echo  isset($_SESSION['login']) ? 'Logout':'Login';?>"><span><i class="glyphicon glyphicon-off <?php echo  isset($_SESSION['login']) ? 'off':'on';?>"></i><?php echo  isset($_SESSION['login']) ? 'Logout':'Login';?></span></a>
                        
                    </li>
                    
               
                </ul>
                
            </div>
        </nav>

        </div>
        <!--END TOPBAR-->

        <div id="wrapper">            
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
              <div class="page-title-breadcrumb" id="title-breadcrumb-option-demo">
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                      <div class="content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 input-icon left" style="margin-bottom:10px;"><a href="#"><i class="fa fa-search" ></i></a><input id="input-thingkey" type="text" class="form-control" placeholder="Thing Key or Serial Number"></div>
                                 <div class="col-xs-12 col-sm-2 col-md-2 " style="margin-bottom:10px;"><input id="organization" type="text" class="form-control" value=" m2mzone-test"></div>
                               <div class="col-xs-12 col-sm-8 col-md-8  system_message pull-left" style="margin-bottom:10px;"> <div class="alert alert-info system-message"><strong>Search:</strong> .... for a live demo use for key "357164040613336" or for serial number "stratus-4000" </div></div> 
                            </div>
                            <div class="row">
                               <div class="col-md-12 api-controller"></div>
                            </div> 
                            <div  class="row">
                                    <div class="col-md-6 property_list"></div>
                                    <div class="col-md-6 property_button"></div>
                            </div>

                            <div class="row">
                                    
                                <div  class="graph-container">
                                    
                                </div>

                            </div>
                      </div> 
                    
                    </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
                <div id="footer">
                    <div class="copyright">
                        <a href="#">M2Mzone 2015 all rights reserved</a></div>
                </div>
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
    </div>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/jquery-ui.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="script/html5shiv.js"></script>
    <script src="script/respond.min.js"></script>            
    <script src="script/responsive-tabs.js"></script>        
    
    <!--LOADING SCRIPTS FOR CHARTS-->
    <script src="script/highcharts.js"></script>        
    <script src="script/highcharts-more.js"></script>        
    <script src="script/exporting.js"></script>            
    <script src="script/solid-gauge.js"></script>        
    <script src="script/drilldown.js"></script>            
    <script src="script/bootstrap-toggle.js"></script>  
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_7sWD-o0bYY8cQviHJFHDTrYxJpI5Nh8&sensor=true"> </script>
    <script src="script/main.js"></script>    


</body>
</html>
